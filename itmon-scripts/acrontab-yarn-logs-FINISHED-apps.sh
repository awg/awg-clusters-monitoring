#!/bin/bash - 
#===============================================================================
#
#          FILE: acrontab-yarn-logs-FINISHED-apps.sh
# 
#         USAGE: ./acrontab-yarn-logs-FINISHED-apps.sh cluster_name hostname
# 
#   DESCRIPTION: to run every hour
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 16/03/2017 12:00
#      REVISION:  ---
#===============================================================================

cd $(dirname $0)

ANALYTIX=analytix,http://p01001532965510.cern.ch:8088/ws/v1/cluster/apps
HADALYTIC=hadalytic,http://p01001532067275.cern.ch:8088/ws/v1/cluster/apps
LXHADOOP=lxhadoop,http://p01001533040197.cern.ch:8088/ws/v1/cluster/apps
HADOOPQA=hadoopqa,http://p05151113457183.cern.ch:8088/ws/v1/cluster/apps

ITMONIT_SINK=http://monit-metricsrc-dev-4d5c244fc4:10012/

finishedTimeEnd=`date --date="$(date +'%H:00')" +'%s'`000
finishedTimeBegin=`date --date="$(date +'%H:00' --date '1 hours ago')" +'%s'`000

java -jar parseYarnAppsMetrics/target/YarnMetricsParser-1.0-SNAPSHOT-jar-with-dependencies.jar $ITMONIT_SINK FINISHED,$finishedTimeBegin,$finishedTimeEnd $ANALYTIX $HADALYTIC $LXHADOOP $HADOOPQA
