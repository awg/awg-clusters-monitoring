var express = require('express');
var router = express.Router();
var path = require('path');

// we are inside ./routes/
var views = __dirname + '/../views/';

/* GET home page. */
router.get('/', function(req, res, next) {
    res.sendFile(path.resolve(views+'/index.html'));
});

/* GET ETL procedures list. */
router.get('/about', function(req, res, next) {
    res.sendFile(path.resolve(views+'/about.html'));
});

router.get('/list', function(req, res, next) {
    res.sendFile(path.resolve(views+'/list.html'));
});

router.get('/projects', function(req, res, next) {
    res.sendFile(path.resolve(views+'/projects.html'));
});

router.get('/add', function(req, res, next) {
    res.sendFile(path.resolve(views+'/add.html'));
});

module.exports = router;