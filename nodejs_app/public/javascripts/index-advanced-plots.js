function chord1(data,d3) {

  users = Object.keys(data)
  users.sort(function(u1,u2){ return data[u1].SPARK||0-data[u2].SPARK||0 })
  var matrix = [];

  matrix[0]=Array.apply(null, Array(users.length+2)).map(Number.prototype.valueOf,0);
  matrix[1]=Array.apply(null, Array(users.length+2)).map(Number.prototype.valueOf,0);

  for (i = 0; i < users.length; i++) { 
    //console.log(i, users[i], data[users[i]])
    matrix[i+2]=[data[users[i]]["SPARK"]|0,data[users[i]]["MAPREDUCE"]|0]
    matrix[i+2]=(matrix[i+2]).concat(Array.apply(null, Array(users.length)).map(Number.prototype.valueOf,0))
    matrix[0][i+2]=matrix[i+2][0]
    matrix[1][i+2]=matrix[i+2][1]
  }

  //console.log(matrix)

  var svg = d3.select('div[id="chord1"]').append("svg:svg");

  svg .attr('style', "min-height: 500px; width: 100%")
  
  var width = document.getElementById('chord1').offsetWidth,
      height = document.getElementById('chord1').offsetHeight,
      outerRadius = Math.min(width, height) * 0.5 - 80,
      innerRadius = outerRadius - 4;
  
  //var r1 = height / 2, r0 = r1 - 100;

  var formatValue = d3.formatPrefix(",.0", 1e3);

  var chord = d3.chord()
      .padAngle(0.05)
      .sortSubgroups(d3.descending);

  var arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius);

  var ribbon = d3.ribbon()
      .radius(innerRadius);

  var color = d3.scaleOrdinal()
      .domain(d3.range(4))
      .range(["#c54911", "#1e54a5"]);

  var g = svg.append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
      .datum(chord(matrix));

  var group = g.append("g")
      .attr("class", "groups")
    .selectAll("g")
    .data(function(chords) { return chords.groups; })
    .enter().append("g");

  group.append("path")
      .style("fill", function(d) { return (d.index!==0&&d.index!==1&&"white")||"black"; })
      // .style("stroke", function(d) { return d3.rgb(color(d.index)).darker(); })
      .attr("d", arc);

  var groupTick = group.selectAll(".group-tick")
    .data(function(d) { return groupTicks(d); })
    .enter().append("g")
      .attr("class", "group-tick")
      .attr("transform", function(d) { return "rotate(" + (d.angle * 180 / Math.PI - 90) + ") translate(" + outerRadius + ",0)"; });

  groupTick.append("line")
      .attr("x2", 6);

  groupTick
    .filter(function(d) { return d.value === 0; })
    // .each(function(d) { d.angle = (d.startAngle + d.endAngle) / 2; })
    // .each(function(d) { console.log(d); })
    .append("text")
      .attr("x", 8)
      .attr("dy", ".35em")
      .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
      .attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180) translate(-16)" : null; })
      // .attr("transform", function(d) {
      //         return //"rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
      //             //+ "translate(" + (- 16) + ")"
      //             + (d.angle > Math.PI ? "rotate(180)" : "");
      //       })
      .text(function(d) {  return d.name; });

  g.append("g")
      .attr("class", "ribbons")
    .selectAll("path")
    .data(function(chords) { return chords; })
    .enter().append("path")
      .attr("d", ribbon)
      .style("fill", function(d) { return color(d.source.index); })
      .style("stroke", function(d) { return d3.rgb(color(d.source.index)).darker(); });

  // Returns an array of tick angles and values for a given group and step.
  function groupTicks(d) {
    var k = (d.endAngle - d.startAngle) / d.value;
    return d3.range(0, d.index+1).map(function(value) {
      return { name: d.index===0 && "SPARK" || d.index===1 && "MAPREDUCE" || users[d.index-2],
        value: value, angle: (d.startAngle + d.endAngle) / 2};
    });
  }
}

function chord2(data,d3) {
  users = Object.keys(data)
  users.sort(function(u1,u2){ return data[u1].analytix||0-data[u2].analytix||0+data[u1].hadalytic||0-data[u2].hadalytic||0 })
  var matrix = [];
  matrix[0]=Array.apply(null, Array(users.length+2)).map(Number.prototype.valueOf,0);
  matrix[1]=Array.apply(null, Array(users.length+2)).map(Number.prototype.valueOf,0);
  matrix[2]=Array.apply(null, Array(users.length+2)).map(Number.prototype.valueOf,0);
  for (i = 0; i < users.length; i++) { 
    matrix[i+3]=[data[users[i]]["analytix"]|0,data[users[i]]["hadalytic"]|0,data[users[i]]["lxhadoop"]|0]
    matrix[i+3]=(matrix[i+3]).concat(Array.apply(null, Array(users.length)).map(Number.prototype.valueOf,0))
    matrix[0][i+3]=matrix[i+3][0]
    matrix[1][i+3]=matrix[i+3][1]
    matrix[2][i+3]=matrix[i+3][2]
  }

  var svg = d3.select('div[id="chord2"]').append("svg:svg");
  svg.attr('style', "min-height: 500px; width: 100%")
  var width = document.getElementById('chord2').offsetWidth,
      height = document.getElementById('chord2').offsetHeight,
      outerRadius = Math.min(width, height) * 0.5 - 80,
      innerRadius = outerRadius - 4;
  var formatValue = d3.formatPrefix(",.0", 1e3);
  var chord = d3.chord()
      .padAngle(0.05)
      .sortSubgroups(d3.descending);
  var arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius);
  var ribbon = d3.ribbon()
      .radius(innerRadius);
  var color = d3.scaleOrdinal()
      .domain(d3.range(4))
      .range(["#c5491d", "#b3dd2a", "#1e54aa"]);
  var g = svg.append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
      .datum(chord(matrix));
  var group = g.append("g")
      .attr("class", "groups")
    .selectAll("g")
    .data(function(chords) { return chords.groups; })
    .enter().append("g");
  group.append("path")
      .style("fill", function(d) { return (d.index!==0&&d.index!==1&&d.index!==2&&"white")||"black"; })
      .attr("d", arc);
  var groupTick = group.selectAll(".group-tick")
    .data(function(d) { return groupTicks(d); })
    .enter().append("g")
      .attr("class", "group-tick")
      .attr("transform", function(d) { return "rotate(" + (d.angle * 180 / Math.PI - 90) + ") translate(" + outerRadius + ",0)"; });
  groupTick.append("line")
      .attr("x2", 6);
  groupTick
    .filter(function(d) { return d.value === 0; })
    .append("text")
      .attr("x", 8)
      .attr("dy", ".35em")
      .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
      .attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180) translate(-16)" : null; })
      .text(function(d) {  return d.name; });
  g.append("g")
      .attr("class", "ribbons")
    .selectAll("path")
    .data(function(chords) { return chords; })
    .enter().append("path")
      .attr("d", ribbon)
      .style("fill", function(d) { return color(d.source.index); })
      .style("stroke", function(d) { return d3.rgb(color(d.source.index)).darker(); });
  // Returns an array of tick angles and values for a given group and step.
  function groupTicks(d) {
    var k = (d.endAngle - d.startAngle) / d.value;
    return d3.range(0, d.index+1).map(function(value) {
      return { name: d.index===0 && "ANALYTIX" || d.index===1 && "HADALYTIC" || d.index===2 && "LXHADOOP" || users[d.index-3],
        value: value, angle: (d.startAngle + d.endAngle) / 2};
    });
  }
}

function heatmapDuration(buckets,d3) {
  var svg = d3.select('div[id="heatmap-duration"]').append("svg:svg");
  svg.attr('style', "min-height: 500px; width: 100%; padding-left: 35px");

  var width = document.getElementById('heatmap-duration').offsetWidth,
      height = document.getElementById('heatmap-duration').offsetHeight;
  var margin = {top: 20, right: 120, bottom: 30, left: 50},
      width = width - margin.left - margin.right,
      height = height - margin.top - margin.bottom;
  var parseDate = d3.timeParse("%Y-%m-%d"),
      formatDate = d3.timeFormat("%b %d");
  var x = d3.scaleTime().range([0, width]),
      y = d3.scaleLinear().range([height, 0]),
      z = d3.scaleLinear().range(["white", "steelblue"]);
  var xStep = 864e5, yStep = 1;
  svg.attr("width", width + margin.left + margin.right)
     .attr("height", height + margin.top + margin.bottom)
    .append("g")
     .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  buckets.forEach(function(d) {
    d.date = parseDate(d.date);
    d.bucket = d.bucket;
    d.count = Math.log10(d.count);
  });
  x.domain(d3.extent(buckets, function(d) { return d.date; }));
  y.domain(d3.extent(buckets, function(d) { return d.bucket; }));
  z.domain([0, d3.max(buckets, function(d) { return d.count; })]);

  x.domain([x.domain()[0], +x.domain()[1] + xStep]);
  y.domain([y.domain()[0], y.domain()[1] + yStep]);
  svg.selectAll(".tile")
     .data(buckets)
    .enter().append("rect")
     .attr("class", "tile")
     .attr("x", function(d) { return x(d.date); })
     .attr("y", function(d) { return y(d.bucket + yStep); })
     .attr("width", x(xStep) - x(0))
     .attr("height",  y(0) - y(yStep))
     .style("fill", function(d) { return z(d.count||1); });
  var legend = svg.selectAll(".legend")
    .data(z.ticks(6).slice(1).reverse())
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(" + (width + 20) + "," + (20 + i * 20) + ")"; });
  legend.append("rect")
    .attr("width", 20)
    .attr("height", 20)
    .style("fill", z);
  function parseText(number){
    return Math.round(Math.pow(10,number-0.5))+"-"+Math.round(Math.pow(10,number))
  }
  legend.append("text")
    .attr("x", 26)
    .attr("y", 10)
    .attr("dy", ".35em")
    .text(parseText);
  svg.append("text")
    .attr("class", "label")
    .attr("x", width + 20)
    .attr("y", 10)
    .attr("dy", ".35em")
    .text("Count");
  // Add an x-axis with label.
  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x).tickFormat(formatDate))
  // Add a y-axis with label.
  svg.append("g")
    .attr("class", "y axis")
    .call(d3.axisLeft(y).tickFormat(function(x){return x+" h"}))
  return true
}
