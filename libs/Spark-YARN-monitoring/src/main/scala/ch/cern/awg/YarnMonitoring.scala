package ch.cern.awg

import java.io.IOException
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.functions.floor
import org.apache.spark.sql.functions.month
import org.apache.spark.sql.functions.year
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import org.apache.spark.storage.StorageLevel

object YarnMonitoring {

  def main(args: Array[String]) {

    var logger = Logger.getLogger(YarnMonitoring.this.getClass())

    // E.g. for analytix:
    // --class YarnMonitoring analytix /project/awg/yarn-apps-history/test/analytix /project/awg/yarn-apps-history/apps /project/awg/yarn-apps-history/ 1455395356
    // Inside outputFolder=args(2) there will be stored the aggregation in Parquet for the selected cluster
    //      -> outputFolder/cluster=clusterName (/project/awg/yarn-apps-history/cluster=analytix)
    // the temporary directory needed to perform the aggregation will be stored in args(3)
    //      -> /project/awg/yarn-apps-history/tmp-analytix-1455395356 # with args(4) as timestamp
    if (args.length < 5) {
      logger.error("=> wrong parameters number")
      System.err.println("Usage: YarnMonitoring <0: cluster name> <1:input JSON logs> " +
        "<2: output Parquet> <3: temp folder path> <4: timestamp>")
      System.exit(1)
    }

    val clusterName = args(0)
    val jsonLogsPath = args(1)
    val outputFolderName = args(2)
    val outputFolderPath = outputFolderName + s"/cluster=$clusterName"
    val tempPath = args(3)
    val timestampExecution = args(4)
    val jobName = s"YarnMonitoring $clusterName aggregation"
    val tableName = s"yarn_apps_$clusterName"

    val tempFolderPath = s"$tempPath/tmp-$clusterName-$timestampExecution"
    val tempLogsFolderPath = s"$tempFolderPath/logs"

    val conf = new SparkConf().setAppName(jobName)
    val sc = new SparkContext(conf) // sparkContext
    val sqlc = new HiveContext(sc) // sqlContext

    logger.info(s"=> jobName '$jobName'")
    logger.info(s"=> clusterName '$clusterName'")
    logger.info(s"=> jsonLogsPath '$jsonLogsPath'")
    logger.info(s"=> outputPath '$outputFolderName'")
    logger.info(s"=> outputFolder '$outputFolderPath'")
    logger.info(s"=> timestampExecution '$timestampExecution'")
    logger.info(s"=> tableName '$tableName'")
    logger.info(s"=> tempFolderPath '$tempFolderPath'")
    logger.info(s"=> tempLogsFolderPath '$tempLogsFolderPath'")

    val fs = FileSystem.get(sc.hadoopConfiguration)

    // create cluster's output folder and temp folder, e.g.
    // => "/project/awg/yarn-apps-history/cluster=analytix"
    // => "/project/awg/yarn-apps-history/tmp-analytix-1488975498
    // => "/project/awg/yarn-apps-history/tmp-analytix-1488975498/logs
    // has semantics of "hadoop fs -mkdir -p"
    try {
      fs.mkdirs(new Path(outputFolderPath))
      logger.info(s"=> folder '$outputFolderPath' created")
      fs.mkdirs(new Path(tempLogsFolderPath))
      logger.info(s"=> temp folder '$tempLogsFolderPath' created")
    } catch {
      case e: IOException => {
        logger.error(s"=> IOException: Cannot create folder $outputFolderPath or $tempLogsFolderPath")
        System.err.println(s"IOException: Cannot create folder $outputFolderPath or $tempLogsFolderPath")
        System.exit(1)
      }
    }

    // To create a new Yarn logs dataset, merging JSON logs and old aggregated YARN data in Parquet.
    // This requires to (1) move JSON logs in temp folder (2) write aggregation in temp folder
    // (3) replace old aggregation

    try {
      val listLogs = fs.listStatus(new Path(jsonLogsPath))
      logger.info(s"=> processing ${listLogs.length} files from $jsonLogsPath")
      if (listLogs.length == 0) {
        logger.error(s"=> folder $jsonLogsPath doesn't contain any files")
        System.err.println(s"=> folder $jsonLogsPath doesn't contain any files")
        System.exit(1)
      }
      listLogs.foreach(filestatus => {
        if (filestatus.isFile()) {
          fs.rename(filestatus.getPath(), new Path(tempLogsFolderPath))
        }
      })
      logger.info(s"=> moved all files from $jsonLogsPath into $tempLogsFolderPath")
    } catch {
      case e: IOException => {
        logger.error(s"=> IOException: Cannot move files from $jsonLogsPath into $tempLogsFolderPath")
        System.err.println(s"IOException: Cannot move files from $jsonLogsPath into $tempLogsFolderPath")
        System.exit(1)
      }
    }

    import sqlc.implicits._
    import org.apache.spark.sql.functions._

    val schema = StructType(StructField("allocatedMB", LongType, true) :: StructField("allocatedVCores", LongType, true) :: StructField("amContainerLogs", StringType, true) :: StructField("amHostHttpAddress", StringType, true) :: StructField("applicationTags", StringType, true) :: StructField("applicationType", StringType, true) :: StructField("clusterId", LongType, true) :: StructField("diagnostics", StringType, true) :: StructField("elapsedTime", LongType, true) :: StructField("finalStatus", StringType, true) :: StructField("finishedTime", LongType, true) :: StructField("id", StringType, true) :: StructField("logAggregationStatus", StringType, true) :: StructField("memorySeconds", LongType, true) :: StructField("name", StringType, true) :: StructField("numAMContainerPreempted", LongType, true) :: StructField("numNonAMContainerPreempted", LongType, true) :: StructField("preemptedResourceMB", LongType, true) :: StructField("preemptedResourceVCores", LongType, true) :: StructField("progress", DoubleType, true) :: StructField("queue", StringType, true) :: StructField("runningContainers", LongType, true) :: StructField("startedTime", LongType, true) :: StructField("state", StringType, true) :: StructField("trackingUI", StringType, true) :: StructField("trackingUrl", StringType, true) :: StructField("user", StringType, true) :: StructField("vcoreSeconds", LongType, true) :: StructField("insertion_ts", LongType, true) :: Nil)

    // load old aggregation
    val tempJSONLogs = sqlc.read.schema(schema)
      .json(tempLogsFolderPath)
      .withColumn("year", year((floor($"startedTime") / 1000).cast("timestamp")))
      .withColumn("month", month((floor($"startedTime") / 1000).cast("timestamp")))

    tempJSONLogs.persist(StorageLevel.MEMORY_AND_DISK)
    
    var resultFlags: Array[((Int, Int), Boolean)] = Array()

    val listMonthsToMerge = tempJSONLogs.where($"id".isNotNull).select("month", "year").distinct().map { case Row(month: Int, year: Int) => (month, year) }.collect()

    logger.info(s"=> Months/Years to process: ${listMonthsToMerge.mkString(", ")}")

    listMonthsToMerge
      .foreach {
        case (monthNumber, yearNumber) => {
          logger.info(s"=> processing year=$yearNumber, month=$monthNumber")
          val currentMonthFolder = s"$outputFolderPath/year=$yearNumber/month=$monthNumber"
          val outputTempFolder = s"$tempFolderPath/aggregated_$yearNumber-$monthNumber"
          try {
            sqlc.read.parquet(currentMonthFolder)
              .unionAll(tempJSONLogs.where(s"year=$yearNumber").where(s"month=$monthNumber").drop("month").drop("year"))
              .dropDuplicates(Array("id", "state", "allocatedMB", "allocatedVCores"))
              .repartition(1)
              .write.parquet(outputTempFolder)
          } catch {
            case e: java.lang.AssertionError => {
              logger.warn(s"=> java.lang.AssertionError: [$currentMonthFolder] doesn't exists, creating one")
              tempJSONLogs.where(s"year=$yearNumber").where(s"month=$monthNumber")
                .drop("month").drop("year")
                .dropDuplicates(Array("id", "state", "allocatedMB", "allocatedVCores"))
                .repartition(1)
                .write.parquet(outputTempFolder)
            }
            case e: IOException => {
              logger.error(s"=> IOException: Cannot read/write folder $currentMonthFolder or $outputTempFolder")
              System.err.println(s"=> IOException: Cannot read/write folder $currentMonthFolder or $outputTempFolder")
              System.exit(1)
            }
          }

          logger.info(s"=> Aggregated result for cluster=$clusterName, year=$yearNumber, month=$monthNumber, written into $outputTempFolder")

          if (fs.isFile(new Path(s"$outputTempFolder/_SUCCESS"))) {
            logger.info(s"=> deleting $currentMonthFolder")
            if (!fs.delete(new Path(currentMonthFolder), true)) {
              logger.info(s"=> creating new folder [$currentMonthFolder] (needed if new month or year)")
              fs.mkdirs(new Path(currentMonthFolder.substring(0, currentMonthFolder.lastIndexOf("/"))))
            }
            logger.info(s"=> renaming [$outputTempFolder] into [$currentMonthFolder]")
            if (fs.rename(new Path(outputTempFolder), new Path(currentMonthFolder))) {
              resultFlags = resultFlags :+ ((yearNumber, monthNumber), true)
            } else {
              logger.error(s"=> could not rename (move) [$outputTempFolder] into [$currentMonthFolder]")
              resultFlags = resultFlags :+ ((yearNumber, monthNumber), false)
            }
          } else {
            logger.error(s"=> (cluster=$clusterName, year=$yearNumber, month=$monthNumber) Unable to write into [$outputTempFolder], " +
              "writing parquet operation wasn't marked as succeeded.")
            resultFlags = resultFlags :+ ((yearNumber, monthNumber), false)
          }

        }
      }

    if (!resultFlags.forall(_._2 == true)) {
      logger.error(s"=> errors with following years/months: ${resultFlags.filter(_._2 == false).map(_._1)}")
      logger.info(s"=> moving back all logs from $tempLogsFolderPath into $jsonLogsPath")
      fs.listStatus(new Path(tempLogsFolderPath)).foreach(filestatus => { if (filestatus.isFile()) fs.rename(filestatus.getPath(), new Path(jsonLogsPath)) })
    } else {
      // Delete REST yarn applications JSON logs only if merged folder is successfully renamed.
      // This will help in case of IOExceptions.
      logger.info(s"=> deleting $tempFolderPath/*")
      if (!fs.delete(new Path(s"$tempFolderPath"), true)){
        logger.warn(s"=> impossible to delete $tempFolderPath")
      }
    }

  }
}
