"""
## IT Hadoop cluster statistics
This DAG will issue wassh commands from aiadm to obtain specs information from the 3 Hadoop clusters
"""
from airflow import DAG
from airflow.operators import DummyOperator
from airflow.operators import PythonOperator
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta
import requests
import json

now = datetime.now()

default_args = {
    'owner': 'lmeniche',
    'depends_on_past': False,
    'start_date': datetime(2017, 3, 21, 6, 0, 0),
    'email': ['lmeniche@cern.ch'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(hours=1)
}

dag = DAG('wassh_clusters_spec_v1.3', default_args=default_args, schedule_interval=timedelta(hours=6))
dag.doc_md = __doc__


clusters_names = [['analytix', 'aimon'], ['hadalytic', 'itdb'], ['lxhadoop', 'default']]
clusters_jmx = [['analytix', 'http://p01001532965510.cern.ch:50070'], ['hadalytic', 'http://p01001532067275.cern.ch:50070'], ['lxhadoop', 'http://p01001533040197.cern.ch:50070']]

command_template = "kinit -k -t /home/lmeniche/private/lmeniche.keytab lmeniche@CERN.CH; \
         ssh -oStrictHostKeyChecking=no -K lmeniche@aiadm7 \
         'wassh -t 300 -c hadoop/{{ params.hostgroup_name }}/datanode \"{{ params.command }}\"' \
         > {{ params.output_filename }}"

filesMap = {}

join = DummyOperator(
    task_id='join',
    trigger_rule='all_success',
    dag=dag
)

for [cluster_name, hostgroup_name] in clusters_names:

    filesMap[cluster_name] = \
        [output_filename_mem,
         output_filename_desc,
         output_filename_hdd,
         output_filename_vcores,
         output_filename_storage] = \
        ['/tmp/'+hostgroup_name+'_datanodes_totalMemory.tsv',
         '/tmp/'+hostgroup_name+'_datanodes_description.tsv',
         '/tmp/'+hostgroup_name+'_datanodes_totalHDD.tsv',
         '/tmp/'+hostgroup_name+'_datanodes_totalVCores.tsv',
         '/tmp/'+hostgroup_name+'_datanodes_totalStorage.tsv']

    download_mem = BashOperator(
        task_id='wassh_mem_'+cluster_name,
				execution_timeout=timedelta(minutes=5),
        params={'command': 'cat /proc/meminfo | grep MemTotal',
                'hostgroup_name': hostgroup_name,
                'output_filename': output_filename_mem},
        bash_command=command_template,
        dag=dag)

    download_desc = BashOperator(
        task_id='wassh_desc_'+cluster_name,
				execution_timeout=timedelta(minutes=5),
        params={'command': 'lsb_release -d',
                'hostgroup_name': hostgroup_name,
                'output_filename': output_filename_desc},
        bash_command=command_template,
        dag=dag)

    download_hdd = BashOperator(
        task_id='wassh_hdd_'+cluster_name,
 				execution_timeout=timedelta(minutes=5),
        params={'command': 'ls /dev/disk/by-label/ | wc -l',
                'hostgroup_name': hostgroup_name,
                'output_filename': output_filename_hdd},
        bash_command=command_template,
        dag=dag)

    download_vcores = BashOperator(
        task_id='wassh_vcores_'+cluster_name,
				execution_timeout=timedelta(minutes=5),
        params={'command': 'lscpu | egrep \\\"(^CPU\(s\)|Thread)\\\" | sed \\\"s/\\s//g\\\" | awk -F: \\\"{print \\\\\$2}\\\" | awk \\\"BEGIN { product = 1 } { product *= \\\\\$1 } END { print product }\\\" ',
                'hostgroup_name': hostgroup_name,
                'output_filename': output_filename_vcores},
        bash_command=command_template,
        dag=dag)

    download_storage = BashOperator(
        task_id='wassh_storage_'+cluster_name,
				execution_timeout=timedelta(minutes=5),
        params={'command': 'df -P |  awk \\\"{print \\\\\$2}\\\" |  awk \\\"{s+=\\\\\$1} END {print s}\\\"',
                'hostgroup_name': hostgroup_name,
                'output_filename': output_filename_storage},
        bash_command=command_template,
        dag=dag)

    download_mem.set_downstream(download_desc)
    download_desc.set_downstream(download_hdd)
    download_hdd.set_downstream(download_vcores)
    download_vcores.set_downstream(download_storage)
    download_storage.set_downstream(join)


def checkFiles(ds, filesMap, **kwargs):
    for [cluster_name, hg] in clusters_names:
        totalLines = []
        for file in filesMap[cluster_name]:
            currentTotal = 0
            with open(file) as wassh_output:
                for line in wassh_output.readlines():
                    currentTotal += 1
                totalLines.append(currentTotal)
        if not all(i == totalLines[0] for i in totalLines):
            raise Exception('Files have no uniform number of lines.')
    return 0

def parseHDFSInfo(ds, **kwargs):
    for [cluster_name, hdfs_info_API] in clusters_jmx:
        result = {}
        for [jmx_name, jmx_path] \
            in [['FSNamesystem', '/jmx?qry=Hadoop:service=NameNode,name=FSNamesystem'],
                ['NameNodeInfo', '/jmx?qry=Hadoop:service=NameNode,name=NameNodeInfo']]:
            r = requests.get(hdfs_info_API+jmx_path)
            result[jmx_name] = json.loads(r.text)['beans'][0]
        output_filename = "/tmp/%s_namenode_stats.json" % cluster_name
        with open(output_filename, 'w') as out:
            json.dump(result, out)

namenode_stats = PythonOperator(
    task_id='namenode_stats',
    python_callable=parseHDFSInfo,
    provide_context=True,
    dag=dag)

check = PythonOperator(
    task_id='check',
    python_callable=checkFiles,
    provide_context=True,
    op_kwargs={'filesMap': filesMap},
    dag=dag)

total_list_files = reduce(lambda x, y: x+y, filesMap.values())

namenode_stats_files = []

for [cluster_name, hdfs_info_API] in clusters_jmx:
    namenode_stats_files.append("/tmp/%s_namenode_stats.json" % cluster_name)

total_list_files += namenode_stats_files

another_join = DummyOperator(
    task_id='join_before_put',
    trigger_rule='all_success',
    dag=dag
)

output_projects_files = '/tmp/consolidated.json'
download_projects = BashOperator(
    task_id='download_projects',
    params={'output_projects_files': output_projects_files},
    bash_command="kinit -k -t /home/lmeniche/private/lmeniche.keytab lmeniche@CERN.CH; \
    scp lmeniche@p01001532965510:/afs/cern.ch/project/hadoop/work/acct/consolidated.json {{ params.output_projects_files }}",
    dag=dag)

total_list_files += [output_projects_files]

move_files = BashOperator(
    task_id='move_files',
    params={'files': " ".join(total_list_files)},
    bash_command="kinit -k -t /home/lmeniche/private/lmeniche.keytab lmeniche@CERN.CH; \
    scp {{ params.files }} lmeniche@awg-dashboard:/home/dashboards/awg-clusters-monitoring/nodejs_app/data",
    dag=dag)


join.set_downstream(check)
check.set_downstream(another_join)
namenode_stats.set_downstream(another_join)
download_projects.set_downstream(another_join)
another_join.set_downstream(move_files)
