#!/bin/bash - 
#===============================================================================
#
#          FILE: yarn-monitoring-acrontab.sh
#
#         USAGE: ./yarn-monitoring-acrontab.sh
#
#   DESCRIPTION: 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 02/11/2016 10:00
#      REVISION: ---
#===============================================================================

cd $(dirname $0)

GIT_BRANCH_NAME="master" 

# Create new log file
LOG_FILE=yarn-monitoring-acrontab-$(date +'%Y%m%d').log
echo "(yarn-monitoring-acrontab.sh) ACRONTAB started at $(date +'%Y%m%d %H:%M:%S')" >> $LOG_FILE

./libs/update-git-and-mvn-build.sh $GIT_BRANCH_NAME "Spark-YARN-monitoring"

./yarn-logs-spark-aggregation.sh $(date +'%s') $LOG_FILE

# send email if log contains errors
./libs/send_mail_after_import.sh $LOG_FILE $(date +'%Y%m%d')

exit 0
