# awg-clusters-monitoring

This repo contains the web application that shows aggregated logs and statistics provided by Spark.

## Description
YARN REST API are available at the URI:

```md
http://cluster-namenode:8088/ws/v1/cluster/apps
```

This will return the last 10K submitted jobs in a JSON document. Applications information are converted in a Spark DataFrame and stored in parquet. To collect periodically these data for all clusters, Airflow tasks are scheduled and acrontabs launch Spark Jobs.

Airflow scripts are present in the ``./libs/airflow`` folder, while the Spark job in charge of processing data per cluster is available at ``./libs/Spark-YARN-monitoring``.

Airflow probes the cluster using ``wassh`` scripts and REST API, storing result in HDFS. The Spark jobs aggregates job records and stores statistic files locally to be displayed by the NodeJS application.

Airflow uses ``./logs/`` folder and Spark aggregates into ``./apps/`` folders:

```bash
$ hadoop fs -ls /project/awg/yarn-apps-history/*
Found 3 items
drwxrwxr-x+  - lmeniche c3          0 2017-01-01 00:31 /project/awg/yarn-apps-history/apps/cluster=analytix
drwxr-xr-x+  - lmeniche c3          0 2017-01-01 01:05 /project/awg/yarn-apps-history/apps/cluster=hadalytic
drwxr-xr-x+  - lmeniche c3          0 2017-01-01 00:37 /project/awg/yarn-apps-history/apps/cluster=lxhadoop
Found 3 items
drwxrwxr-x+  - lmeniche c3          0 2017-03-22 17:31 /project/awg/yarn-apps-history/logs/analytix
drwxrwxr-x+  - lmeniche c3          0 2017-03-22 17:33 /project/awg/yarn-apps-history/logs/hadalytic
drwxrwxr-x+  - lmeniche c3          0 2017-03-22 17:36 /project/awg/yarn-apps-history/logs/lxhadoop
```

## Node JS

On the web host server node, ``pm2`` is running the application pointing one folder below the git repository. YAML file should contain:

```md
apps:

  - name   : awg_monit
    script : ./awg-clusters-monitoring/nodejs_app/./bin/www
    watch  : true
    env    :
      NODE_ENV: development
      ANALYTIX_NAMENODE: p01001532965510.cern.ch
      PORT: 8089
    env_production:
      NODE_ENV: production
      PORT: 8089
    exec_mode: simple
    cwd: ./awg-clusters-monitoring/nodejs_app/
```
and one command to start the webpage may be:

```bash
pm2 start process.yml --only awg_monit
```

## Screenshots:

![](Dashboard_example.png)