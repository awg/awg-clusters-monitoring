package ch.cern.awg;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequest;

/**
 * 
 */
public class App {

	private synchronized static void printErrorMessate(Exception e, String clusterName) {
		System.err.println("ERROR while HTTP GET info for cluster " + clusterName);
		System.err.println("Exception: " + e.getClass().getName());
		System.err.println("Message: " + e.getMessage());
	}

	public static void main(String[] args) throws IOException {

		long currentTS = Calendar.getInstance().getTimeInMillis();

		Map<String, String> clusterName2YarnURL = new HashMap<String, String>(
				args.length);

		String sinkURL = args[0];
		
		boolean isRunning = true;
		
		long finishedTimeEnd = 0L;
		long finishedTimeBegin = 0L;
		
		if (!args[1].equals("RUNNING")){
			isRunning=false;
			finishedTimeBegin = Long.parseLong(args[1].split(",")[1]);
			finishedTimeEnd = Long.parseLong(args[1].split(",")[2]);
		}

		for (int i = 2; i < args.length; i++) {
			String[] cluster2URL = args[i].split(",");
			clusterName2YarnURL.put(cluster2URL[0], cluster2URL[1]);
		}

		Unirest.setTimeouts(10000L, 30000L);

		Map<String, Future<HttpResponse<JsonNode>>> futuresMap = 
				new HashMap<String, Future<HttpResponse<JsonNode>>>(clusterName2YarnURL.size());

		for (String clusterName : clusterName2YarnURL.keySet()) {
			HttpRequest request = Unirest.get(clusterName2YarnURL.get(clusterName));
			if (isRunning){
				request = request.queryString("state", "RUNNING");
			} else {
				request = request.queryString("finishedTimeBegin", finishedTimeBegin);
				request = request.queryString("finishedTimeEnd", finishedTimeEnd);
			}
			futuresMap.put(clusterName,request.asJsonAsync());
		}

		ArrayList<JSONObject> appsArray = new ArrayList<JSONObject>(
				clusterName2YarnURL.size());

		for (String clusterName : futuresMap.keySet()) {
			try {
				HttpResponse<JsonNode> response = futuresMap.get(clusterName).get(50L, TimeUnit.SECONDS);
				appsArray.add(response.getBody().getObject().put("type", clusterName));
			} catch (InterruptedException e) {
				printErrorMessate(e,clusterName);
			} catch (ExecutionException e) {
				printErrorMessate(e,clusterName);
			} catch (CancellationException e) {
				printErrorMessate(e,clusterName);
			} catch (JSONException e) {
				printErrorMessate(e,clusterName);
			} catch (TimeoutException e) {
				printErrorMessate(e,clusterName);
			}
		}

		ArrayList<JSONObject> applications = new ArrayList<JSONObject>(200);

		for (JSONObject jsonNode : appsArray) {
			String type = jsonNode.getString("type");
			if (jsonNode.isNull("apps")){
				continue;
			}
			JSONArray apps;
			try {
				apps = jsonNode.getJSONObject("apps").getJSONArray("app");
			} catch (JSONException e1) {
				System.err.println("ERROR while parsing JSON for cluster: " + type);
				System.err.println("Exception: " + e1.getClass().getName());
				System.err.println("Message: " + e1.getMessage());
				continue;
			}
			for (int i = 0; i < apps.length(); i++) {
				JSONObject app = apps.getJSONObject(i).put("type", type);
				app.put("producer", "hadoop");
				app.remove("diagnostics");
				app.remove("applicationTags");
				app.remove("logAggregationStatus");
				app.remove("trackingUI");
				if (app.getString("applicationType").equals("SPARK"))
					app.remove("progress");

				if (!app.getString("finalStatus").equals("UNDEFINED")) {
					app.remove("allocatedMB");
					app.remove("allocatedMB");
					app.remove("runningContainers");
					app.remove("allocatedVCores");
				}
				if (!app.getString("finalStatus").equals("FINISHED")) {
					app.remove("progress");
				}
				app.put("timestamp", currentTS);

				if (app.getLong("finishedTime") != 0) {
					app.put("timestamp", app.getLong("finishedTime"));
				}

				try {
					app.put("hostname", InetAddress.getLocalHost().getHostName());
				} catch (UnknownHostException e) {
					app.put("hostname", "unknown");
				}
				
				app.put("path", "apps");
				app.put("type_prefix", "raw");

				applications.add(app);
			}
		}

		HttpResponse<JsonNode> jsonResponse = null;
		try {
			jsonResponse = Unirest.post(sinkURL)
					.header("accept", "application/json")
					.body(applications.toString().trim())
					.asJson();
						
			if (jsonResponse.getStatus() != 200) {
				System.err.println("ERROR, exit status not OK: " + jsonResponse.getStatus());
				System.err.println("Reason: " + jsonResponse.getStatusText());
			}
						
		} catch (UnirestException e) {
			System.err.println("ERROR while HTTP POST to "+ sinkURL);
			System.err.println("Exception: " + e.getClass().getName());
			System.err.println("Message: " + e.getMessage());
			System.out.println(isRunning ? "RUNNING jobs, timestamp: "+currentTS : 
				"FINISHED jobs, timestamp begin: "+finishedTimeBegin+", timestamp end:"+finishedTimeEnd );
			System.out.println("Apps list: " + applications.toString());
		} catch (JSONException e) {
			System.err.println("ERROR while parsing JSON before POST ");
			System.err.println("Exception: " + e.getClass().getName());
			System.err.println("Message: " + e.getMessage());
		}
		
		Unirest.shutdown();

	}
}
