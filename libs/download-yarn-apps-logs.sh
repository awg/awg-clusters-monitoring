#!/bin/bash - 
#===============================================================================
#
#          FILE: download-yarn-apps-logs.sh
# 
#         USAGE: ./download-yarn-apps-logs.sh
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 07/11/2016 16:08
#      REVISION:  ---
#===============================================================================

clusters=(analytix,p01001532965510 hadalytic,p01001532067275 lxhadoop,p01001533040197)

tmp_folder=$1
logs_folder=$2

for i in ${clusters[*]}; do
    cluster=$(echo $i | tr "," "\n" | head -n1)
    namenode=$(echo $i | tr "," "\n" | tail -n1)
    OUTPUT=$tmp_folder/yarn-apps-$cluster-$(date +'%s').json;
    wget -q http://$namenode.cern.ch:8088/ws/v1/cluster/apps -O $OUTPUT
    mv $OUTPUT $logs_folder
done