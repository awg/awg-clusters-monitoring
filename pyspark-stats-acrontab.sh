#!/bin/bash -
#===============================================================================
#
#          FILE: pyspark-compute-stats.sh
#
#         USAGE: ./pyspark-compute-stats.sh [-s: optional flag]
#
#   DESCRIPTION: if the -s flag is present, do not refresh all statistics
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 14/11/2016 18:07
#       VERSION: 1.0.0
#===============================================================================

if [ -z $1 ]; then
    computeLongStats="True"
else
    computeLongStats="False"
fi

YARN_HISTORY_PATH="/project/awg/yarn-apps-history/apps/"

SPARK_LOG_DIR=spark_logs
mkdir $SPARK_LOG_DIR 2>/dev/null
SPARK_LOGS=$SPARK_LOG_DIR/spark-yarn-stats-$(date +'%Y%m%d-%H:%M').log

YARN_APP_STATUS_SCRIPT=$PWD/libs/check-yarn-job-status.sh
YARN_MASTER=p01001532965510.cern.ch:8088

spark-submit \
    --name "YARN monitoring stats" \
    --driver-class-path '/usr/lib/hive/lib/*' \
    --driver-java-options '-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*' \
    libs/pyspark-index-stats.py $computeLongStats $YARN_HISTORY_PATH \
    >> $SPARK_LOGS 2>&1

sleep 1m

application_ID=$(cat $SPARK_LOGS | grep "Submitted application" | tr " " "\n" | grep application_ | tail -n1)
if [ "$application_ID" == "" ]; then
    echo "ERROR: no applicationId found, impossible to verify if job execution was successful"
else
    # checks if previous submitted application has finalStatus == SUCCEEDED
    $YARN_APP_STATUS_SCRIPT $YARN_MASTER $application_ID "SUCCEEDED" 2>&1 1>/dev/null
    if [ $? != 0 ]; then
        echo "ERROR: an error occurred during [$SELECTED_STEP]," \
            "the job SPARK with ID ($application_ID) was NOT marked as SUCCEEDED"
    else
        echo "SPARK job (application_ID=$application_ID) is marked with state or finalStatus SUCCEEDED"
    fi
fi
