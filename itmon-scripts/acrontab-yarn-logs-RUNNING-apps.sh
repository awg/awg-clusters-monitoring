#!/bin/bash - 
#===============================================================================
#
#          FILE: acrontab-yarn-logs-RUNNING-apps.sh
# 
#         USAGE: ./acrontab-yarn-logs-RUNNING-apps.sh
# 
#   DESCRIPTION: to run every minute
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 16/03/2017 12:00
#      REVISION:  ---
#===============================================================================

cd $(dirname $0)

ANALYTIX=analytix,http://p01001532965510.cern.ch:8088/ws/v1/cluster/apps
HADALYTIC=hadalytic,http://p01001532067275.cern.ch:8088/ws/v1/cluster/apps
LXHADOOP=lxhadoop,http://p01001533040197.cern.ch:8088/ws/v1/cluster/apps
HADOOPQA=hadoopqa,http://p05151113457183.cern.ch:8088/ws/v1/cluster/apps

ITMONIT_SINK=http://monit-metrics.cern.ch:10012/

java -jar parseYarnAppsMetrics/target/YarnMetricsParser-1.0-SNAPSHOT-jar-with-dependencies.jar $ITMONIT_SINK RUNNING $ANALYTIX $HADALYTIC $LXHADOOP $HADOOPQA
