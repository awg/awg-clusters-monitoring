#!/bin/bash - 
#===============================================================================
#
#          FILE: send_mail_after_import.sh
# 
#         USAGE: ./send_mail_after_import.sh "log file to parse" "date"
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 15.06.2015 16:24
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

OUTPUT_ERROR=`cat $1 | egrep "ERROR"`

SUBJECT="Report YARN apps monitoring [$2] errors"
MAIL=`cat ~/.forward`

if [[ $OUTPUT_ERROR == *"ERROR"* ]]
then
	(echo "Check file [$1] for more info." && echo "===========" && echo "${OUTPUT_ERROR}") | mail -s "$SUBJECT" $MAIL
fi

