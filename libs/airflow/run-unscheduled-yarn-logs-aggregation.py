"""
## Run unscheduled Tasks

Run those tasks with no status.
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from os import listdir
from os.path import isfile, join, basename
import subprocess as sub

now = datetime.now()

default_args = {
    'owner': 'lmeniche',
    'depends_on_past': False,
    'start_date': datetime(2017, 3, 22, 15, 30, 0),
    'email': ['lmeniche@cern.ch'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=30)
}

yarn_logs_aggregation_version = 'yarn_logs_aggregation_v1.4'

dag = DAG('run_unscheduled_yarn_tasks_v1.0', default_args=default_args, schedule_interval=timedelta(hours=1))
dag.doc_md = __doc__
# mysql -h awg-virtual -u airflow --password=asafepassword airflow  -e
p = sub.Popen(['mysql', '-h', 'awg-virtual', '-u', 'airflow', '--password=asafepassword', 'airflow', '-e', 'select total, REPLACE(execution_date," ","T") execution_date, executed_task_id from (select GROUP_CONCAT(task_id, ",") executed_task_id, execution_date, count(task_id) total from task_instance where dag_id = "yarn_logs_aggregation_v1.4" group by execution_date) T where T.total != 21;'], stdout=sub.PIPE, stderr=sub.PIPE)
output, errors = p.communicate()
lines = output.split("\n")

if len(lines) != 0:
    for line in lines:
        if len(line.split('\t')) >= 3:
            # print line
            items = line.split('\t')
            ts = items[1]
            if (now-timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S') > ts:
                print 'airflow backfill -s %s -e %s %s' % (ts, ts, yarn_logs_aggregation_version)
                reschedule = BashOperator(
                    task_id='reschedule_'+ts.replace(":",""),
                    params={'ts': ts, 'yarn_logs_aggregation_version': yarn_logs_aggregation_version},
                    bash_command='airflow backfill -s {{ params.ts }} -e {{ params.ts }} {{ params.yarn_logs_aggregation_version }}',
                    dag=dag)
