#!/bin/bash - 
#===============================================================================
#
#          FILE: update-git-and-mvn-build.sh
# 
#         USAGE: ./update-git-and-mvn-build.sh branch_name project_path
# 
#   DESCRIPTION: update git and build maven project
# 
#       OPTIONS: ---
#  REQUIREMENTS: git
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 26/08/2016 21:00
#      REVISION: ---
#===============================================================================

cd $(dirname $0)/$2

GIT_BRANCH=$1

git commit -am "automatic commit from $(hostname) @$(date +'%s')"

git remote -v update > /dev/null 2>&1

git checkout $GIT_BRANCH > /dev/null 2>&1

status=$(git status)

if [[ $status == *"Your branch is behind 'origin/$GIT_BRANCH'"* ]] || [[ $status == *"Your branch and 'origin/$GIT_BRANCH' have diverged"* ]] || [[ $status == *"Your branch is ahead of 'origin/$GIT_BRANCH'"* ]]
then
    echo "Pulling changes for branch $GIT_BRANCH"
    git pull origin $GIT_BRANCH
    git push origin $GIT_BRANCH
    if [[ $status != *"Your branch is ahead of 'origin/$GIT_BRANCH'"* ]]; then
        export JAVA_HOME=${JAVA_HOME:-/usr/lib/jvm/jre-1.7.0-oracle.x86_64}
        /usr/local/bin/mvn clean package -DskipTests
    fi
else
    if [ ! -d "target" ]; then
        export JAVA_HOME=${JAVA_HOME:-/usr/lib/jvm/jre-1.7.0-oracle.x86_64}
        /usr/local/bin/mvn clean package -DskipTests
    fi
    echo "Already up-to-date."
fi

cd -
