#!/bin/bash -
#===============================================================================
#
#          FILE: yarn-logs-spark-aggregation.sh
#
#         USAGE: ./yarn-logs-spark-aggregation.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 14/11/2016 18:07
#       VERSION: 1.0.0
#===============================================================================

# $1 = log file name

# input parameters check
if [ -z $1 ] || [ -z $2 ]; then
    echo "ERROR some parameter is missing"
    echo "Usage: ./yarn-logs-spark-aggregation.sh timestamp log_file_name"
    exit 1
fi

VERSION="1.0.0"

TIMESTAMP=$1
LOGFILE=$2

# other variables
YARN_MONITORING_HDFS_PATH=/project/awg/yarn-apps-history/apps
TMP_FOLDER_HDFS_PATH=/project/awg/yarn-apps-history/
JSON_LOGS_PATH=/project/awg/yarn-apps-history/logs/

# logs found in the tmp folder
SPARK_LOG_DIR=spark_logs
mkdir $SPARK_LOG_DIR 2>/dev/null
SPARK_LOGS_PREFIX=$SPARK_LOG_DIR/spark-aggregated-yarn-logs-$TIMESTAMP

# libs
SPARK_JAR=$PWD/libs/Spark-YARN-monitoring/target/*-jar-with-dependencies.jar
SPARK_MAIN=ch.cern.awg.YarnMonitoring

YARN_APP_STATUS_SCRIPT=$PWD/libs/check-yarn-job-status.sh
YARN_MASTER=p01001532965510.cern.ch:8088

# Log functions
echo "(dsi-launch-spark.sh) Starting with parameters $*" >> $LOGFILE
function logsetup { exec > >(tee -a $LOGFILE); exec 2>&1; }
function log_info { echo "[$(date +'%Y%m%d-%H:%M:%S')] INFO: $*"; }
function log_error { echo "[$(date +'%Y%m%d-%H:%M:%S')] ERROR: $*"; }
logsetup

log_info "Starting ./yarn-logs-spark-aggregation.sh $*"

# print version
log_info "version: $VERSION"
JAR_PATH=$(eval echo $SPARK_JAR)
JAR_VERSION=${JAR_PATH##*/}
log_info "jar version: $JAR_VERSION"

clusters=(analytix hadalytic lxhadoop)

log_info "Running aggregation jobs for clusters: ${clusters[*]}"

for cluster in ${clusters[*]}; do
  MASTER="yarn-client"
  APP_NAME="YarnMonitoring $cluster aggregation"
  SPARK_LOGS=$SPARK_LOGS_PREFIX-$cluster.log
  log_info "Starting aggregation for cluster $cluster, master=$MASTER, APP_NAME=$APP_NAME, SPARK_LOGS= $PWD/$SPARK_LOGS"
  spark-submit \
    --name "$APP_NAME" \
    --master "$MASTER" \
    --class $SPARK_MAIN \
    --executor-memory 1G \
    --executor-cores 1 \
    --driver-class-path=/usr/lib/hive/lib/* \
    --conf spark.rpc.askTimeout=560 \
    --driver-java-options="-Dspark.executor.extraClassPath=/usr/lib/hive/lib/*" \
    $SPARK_JAR \
    $cluster \
    $JSON_LOGS_PATH/$cluster \
    $YARN_MONITORING_HDFS_PATH \
    $TMP_FOLDER_HDFS_PATH \
    $TIMESTAMP \
    >> $SPARK_LOGS 2>&1
    sleep 1m
    application_ID=$(cat $SPARK_LOGS | grep "Submitted application" | tr " " "\n" | grep application_ | tail -n1)
    if [ "$application_ID" == "" ]; then
        log_error "no applicationId found, impossible to verify if job execution was successful"
    else
        # checks if previous submitted application has finalStatus == SUCCEEDED
        $YARN_APP_STATUS_SCRIPT $YARN_MASTER $application_ID "SUCCEEDED" 2>&1 1>/dev/null
        if [ $? != 0 ]; then
            log_error "an error occurred during [$SELECTED_STEP]," \
                "the job SPARK with ID ($application_ID) was not marked as SUCCEEDED"
        else
            log_info "SPARK job (application_ID=$application_ID) is marked with state or finalStatus SUCCEEDED"
        fi
    fi

done
