import fileinput
import requests
import json
from time import time
import sys
import socket

def send(document):
    return requests.post('http://monit-metricsrc-dev-4d5c244fc4:10012/', data=json.dumps(document), headers={ "Content-Type": "application/json; charset=UTF-8"})

def send_and_check(document, should_fail=False):
    response = send(document)
    assert( (response.status_code in [200]) != should_fail), 'Status code: {0}. Message: {1}'.format(response.status_code, response.text)

# read from stdin and parse JSON
logs = requests.get(sys.argv[2]).text  # TODO check input

j = json.loads(logs)  # TODO add try/catch

if (j["apps"] == None):
    exit(0)

apps_list = j["apps"]["app"]

# list of logs
documents = []

# parameters
current_timestamp = long(time())*1000
cluster_name = sys.argv[1]  # TODO check input

for data in apps_list:

    data['logType'] = 'yarn-apps'

    data.pop('diagnostics')
    data.pop('logAggregationStatus')
    data.pop('trackingUI')
    if (data['applicationType'] == "SPARK"):
        data.pop('progress')

    if (data['finalStatus'] != "UNDEFINED"):
        data.pop('allocatedMB')
        data.pop('allocatedVCores')
        data.pop('runningContainers')

    timestamp = current_timestamp

    if (data["finishedTime"] != 0 ):
        timestamp = long(data["finishedTime"])

    documents.append({
        'producer': 'hadoop',
        'type_prefix': 'raw',
        'type': cluster_name,
        'hostname': socket.gethostname(),
        'timestamp': timestamp,
        'data': data,
        'path': 'apps'
        })

# print json.dumps(documents)

send_and_check(documents)
