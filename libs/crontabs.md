Crontabs to be installed with no-root user (e.g. ``crontab -u lmeniche -e``):


```
# Yarn Logs 1 of 2
# * * * * * bash ~/yarn-applications-history/libs/download-yarn-apps-logs.sh /home/yarn-logs/tmp /home/yarn-logs/
# Yarn logs 2 of 2
# * * * * * sleep 30; bash ~/yarn-applications-history/libs/download-yarn-apps-logs.sh /home/yarn-logs/tmp /home/yarn-logs/
```

and the acrontab that will run the job:

```
*/30 * * * * awg-jobsubmit.cern.ch cd /home/lmeniche/yarn-applications-history/; ./yarn-monitoring-acrontab.sh
```