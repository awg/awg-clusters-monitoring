#!/bin/bash - 
#===============================================================================
#
#          FILE: check-yarn-job-status.sh
# 
#         USAGE: ./check-yarn-job-status.sh host:port appId state
# 
#   DESCRIPTION: this script checks if the application has a defined state.
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 24/07/2015 12:00
#      REVISION:  ---
#===============================================================================


if [ -z $1 ] || [ -z $2 ] || [ -z $3 ]; then
	echo "ERROR, wrong parameters number."
	echo "USAGE: ./check-yarn-job-status.sh host:port appId state"
	exit 1
fi

output_curl=$(curl "http://$1/ws/v1/cluster/apps/$2/" 2>/dev/null) 

state_uppercase=${3^^}

if [[ $output_curl != *"\"state\":\"$3\""* ]]; then
	if [[ $output_curl != *"\"finalStatus\":\"$3\""* ]]; then
		echo "Neither 'state' nor 'finalState' are equal to $3"
		exit 1
	else
		echo "'finalState' is equal to $3"
	fi
else
	echo "'state' is equal to $3"
fi

exit 0