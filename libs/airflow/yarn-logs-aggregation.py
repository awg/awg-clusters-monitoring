"""
## Yarn applications monitoring logs

This DAG merges YARN logs and put them into a folder from where an acrontab
script will import them into HDFS (analytix).
"""
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from os import listdir
from os.path import isfile, join, basename, getsize
import json

def merge_files(ds, list_of_files, logs_folder, cluster_name, **kwargs):
    files = list_of_files.split(' ')
    with open(logs_folder+"/aggregated-"+cluster_name+"-"+kwargs['run_id']+".json", 'wa') as merged_json:
        for file in files:
            filename = logs_folder+'/'+file
            if getsize(filename) > 0:
                with open(logs_folder+'/'+file) as yarn_apps:
                    try:
                        for app in json.load(yarn_apps)['apps']['app']:
                            app['insertion_ts'] = long(file.split('-')[-1].split('.')[0])
                            merged_json.write(json.dumps(app)+"\n")
                    except ValueError, e:
                        print('ERROR (ValueError) parsing:', filename, 'description:', e)
                    except KeyError, e:
                        print('ERROR (KeyError) unknown JSON keys:', filename, 'description:', e)
                    except TypeError, e:
                        print('ERROR (TypeError)', filename, 'description:', e)

now = datetime.now()

default_args = {
    'owner': 'lmeniche',
    'depends_on_past': False,
    'start_date': datetime(2017, 3, 22, 11, 10, 0),
    'email': ['lmeniche@cern.ch'],
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG('yarn_logs_aggregation_v1.4', default_args=default_args, schedule_interval=timedelta(minutes=10))
dag.doc_md = __doc__

logs_folder = '/home/yarn-logs'
tars_folder = logs_folder + '/tars'
clusters_names = ['analytix', 'hadalytic', 'lxhadoop']

for cluster_name in clusters_names:

    file_filter = 'yarn-apps-'+cluster_name

    hdfs_destination = '/project/awg/yarn-apps-history/logs/'+cluster_name

    list_of_files = [f for f in listdir(logs_folder) if isfile(join(logs_folder, f)) and file_filter in basename(f)]

    if len(list_of_files) != 0:

        list_of_files.sort()
        list_of_files = ' '.join(list_of_files[:100])

        tar_filename_prefix = cluster_name+"-logs-archive"

        tar_path_prefix = logs_folder+"/"+tar_filename_prefix

        merge = PythonOperator(
            task_id='merge_files_'+cluster_name,
            python_callable=merge_files,
            provide_context=True,
            op_kwargs={'list_of_files': list_of_files, 'logs_folder': logs_folder, 'cluster_name': cluster_name},
            dag=dag)

        gzip = BashOperator(
            task_id='create_tar_'+cluster_name,
            params={'cluster_name': cluster_name, 'tar_path_prefix': tar_path_prefix, 'logs_folder': logs_folder},
            bash_command='gzip -c {{ params.logs_folder }}/aggregated-{{ params.cluster_name }}-{{ run_id }}.json > {{ params.tar_path_prefix }}-{{ ts_nodash }}.gz',
            dag=dag)

        move_tar = BashOperator(
            task_id='move_tar_'+cluster_name,
            params={'tar_path_prefix': tar_path_prefix, 'tars_folder': tars_folder},
            bash_command='mv {{ params.tar_path_prefix }}-{{ ts_nodash }}.gz {{ params.tars_folder }}',
            dag=dag)

        hadoop_put = BashOperator(
            task_id='hadoop_put_'+cluster_name,
            params={'tar_filename_prefix': tar_filename_prefix, 'tars_folder': tars_folder, 'hdfs_destination': hdfs_destination},
            bash_command='FILE={{ params.tars_folder }}/{{ params.tar_filename_prefix }}-{{ ts_nodash }}.gz ; kinit awgmgr@CERN.CH -k -t /etc/hadoop/conf/awgmgr.keytab; hadoop fs -put $FILE {{ params.hdfs_destination }}',
            dag=dag)

        remove_tar = BashOperator(
            task_id='remove_tar_'+cluster_name,
            params={'tar_filename_prefix': tar_filename_prefix, 'tars_folder': tars_folder},
            bash_command='FILE={{ params.tars_folder }}/{{ params.tar_filename_prefix }}-{{ ts_nodash }}.gz ; rm -f $FILE',
            dag=dag)

        remove_logs = BashOperator(
            task_id='remove_logs_'+cluster_name,
            params={'files': list_of_files, 'logs_folder': logs_folder},
            bash_command='list="{{ params.files }}"; cd {{ params.logs_folder }}; rm -f $list; cd -',
            dag=dag)

        remove_merged_logs = BashOperator(
            task_id='remove_merged_logs_'+cluster_name,
            params={'logs_folder': logs_folder, 'cluster_name': cluster_name},
            bash_command='rm -f {{ params.logs_folder }}/aggregated-{{ params.cluster_name }}-{{ run_id }}.json',
            dag=dag)

        gzip.set_upstream(merge)
        remove_logs.set_upstream(merge)
        remove_merged_logs.set_upstream(gzip)
        move_tar.set_upstream(gzip)
        hadoop_put.set_upstream(move_tar)
        remove_tar.set_upstream(hadoop_put)
