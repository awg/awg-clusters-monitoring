from pyspark import (SparkConf, SparkContext)
from pyspark.sql import HiveContext
from pyspark import StorageLevel
import json
import re
from pprint import pprint
import time
import sys
import math

if len(sys.argv) < 3:
    print """ERROR: missing a parameter
    ./pyspark-index-stats.py boolean path
        - boolean: true to update all statistics (also those demanding long computation)
        - path: HDFS path where YARN history logs are stored
    """
    exit(1)

print """Launching %s program with the following parameters: %s, %s """ % (sys.argv[0], sys.argv[1],sys.argv[2])

calculateLongStats = False
yarnAppHistoryPath = sys.argv[2]

if sys.argv[1] == "True" or sys.argv[1] == "true":
    calculateLongStats = True

print("Calculate long statistics? %s" % calculateLongStats)


def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

output_file_path = 'nodejs_app/data/index.json'

with open(output_file_path) as file:
    index = json.load(file)

general_stats = index["general_stats"]["data"]
index["general_stats"]["data"] = []


def count_nodes(filename):
    total_nodes = 0
    with open(filename) as f:
        for line in f.readlines():
            total_nodes += 1
    return total_nodes


def sum_memory(filename):
    total_memory_KB = 0
    with open(filename) as f:
        for line in f.readlines():
            total_memory_KB += int(re.split("\s+", line)[2])
    return total_memory_KB


def count_HDDs(filename):
    total_HDDs = 0
    with open(filename) as f:
        for line in f.readlines():
            total_HDDs += int(re.split("\s+", line)[1])
    return total_HDDs


def count_cores(filename):
    total_vcores = 0
    with open(filename) as f:
        for line in f.readlines():
            total_vcores += int(re.split("\s+", line)[1])
    return total_vcores


def sum_storage_space(filename):
    total_storage = 0
    with open(filename) as f:
        for line in f.readlines():
            total_storage += int(re.split("\s+", line)[1])
    return total_storage


index['ts'] = int(round(time.time()))

total_nodes = 6  # 2 namenodes for each cluster
total_memory_KB = 0
total_HDDs = 0
total_vcores = 0
total_storage = 0

for hg in ['aimon', 'default', 'itdb']:
    total_nodes += count_nodes('./nodejs_app/data/%s_datanodes_description.tsv' % hg)
    total_memory_KB += sum_memory('./nodejs_app/data/%s_datanodes_totalMemory.tsv' % hg)
    total_HDDs += count_HDDs('./nodejs_app/data/%s_datanodes_totalHDD.tsv' % hg)
    total_vcores += count_cores('./nodejs_app/data/%s_datanodes_totalVCores.tsv' % hg)
    total_storage += sum_storage_space('./nodejs_app/data/%s_datanodes_totalStorage.tsv' % hg)

index["overview"] = {
    "nodes": total_nodes,
    "vcores": total_vcores,
    "RAM": total_memory_KB,
    "HHD": total_HDDs,
    "space": total_storage
    }

total_blocks = 0
total_files = 0
total_capacity = 0
free_capacity = 0
dn_usage_avg = 0

cluster_names = ['analytix', 'hadalytic', 'lxhadoop']

for cluster_name in cluster_names:
    with open('./nodejs_app/data/%s_namenode_stats.json' % cluster_name) as f:
        j = json.load(f)
        total_files += j['NameNodeInfo']['TotalFiles']
        total_blocks += j['NameNodeInfo']['TotalBlocks']
        total_capacity += j['NameNodeInfo']['Total']
        free_capacity += j['NameNodeInfo']['Free']
        dn_usage_avg += float(json.loads(j['NameNodeInfo']['NodeUsage'])['nodeUsage']['median'][:-1])

used_capacity = (total_capacity - free_capacity)
dn_usage_avg = math.floor(dn_usage_avg*100/len(cluster_names))/100

index["general_stats"]["data"].append({"label": "Total number of files", "value": total_files})
index["general_stats"]["data"].append({"label": "Total number of blocks", "value": total_blocks})
index["general_stats"]["data"].append({"label": "Total HDFS capacity", "value": sizeof_fmt(total_capacity)})
index["general_stats"]["data"].append({"label": "Used HDFS capacity", "value": sizeof_fmt(used_capacity)})
index["general_stats"]["data"].append({"label": "Median datanode usage", "value": str(dn_usage_avg)+" %"})

free_capacity = total_capacity-used_capacity
perc_free = free_capacity*100/total_capacity
perc_used = 100-perc_free

hdfs_usage = [{"label": "% used capacity", "value": perc_used}, {"label":"% free capacity", "value" :perc_free}]
index["hdfs_usage"] = {}
index["hdfs_usage"]["data"] = hdfs_usage


#
# Spark app
#

conf = SparkConf()
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

df = sqlContext.read.parquet(yarnAppHistoryPath).repartition("cluster")
df.persist(StorageLevel.MEMORY_AND_DISK)
df.registerTempTable("yarnHistory")

oneMonthAgoMillis = int(round(time.time() * 1000)-2592000000)
oneDayAgoMillis = int(round(time.time() * 1000)-60*60*24*1000)

#
# Calculate average daily jobs in the last 30 days
#
query = """
    select id, to_date(cast(startedTime/1000 as timestamp)) day
    from yarnHistory where startedTime > cast(%s as bigint)
    group by id, startedTime, cluster
    """ % oneMonthAgoMillis

avg_daily_jobs = int(
    sqlContext.sql(query).groupBy("day").count()
    .groupBy().avg("count").collect()[0].asDict()['avg(count)']
    )

index["overview"]["daily_jobs"] = avg_daily_jobs

#
# Calculate distinct daily jobs for the last 30 days
# grouped by status
#
query = """
    select id, to_date(cast(startedTime/1000 as timestamp)) day, state
    from yarnHistory where startedTime>cast(%s as bigint) and state!='RUNNING' and state!='ACCEPTED' and state!='NEW' 
    group by id, startedTime, state, cluster
    """ % oneMonthAgoMillis


def merge(group):
    result = {z: y for z, y in group[1]}
    result["period"] = group[0]
    return result

jobs_status_30days = sqlContext.sql(query).groupBy("day", "state").count()\
    .withColumnRenamed("count", "total")\
    .map(lambda row: [str(row.day), [str(row.state), str(row.total)]])\
    .groupByKey().map(merge).collect()

index["jobs_status_30days"] = {}
index["jobs_status_30days"]["data"] = jobs_status_30days


#
# Calculate distinct daily jobs for the last 30 days
#
query = """
    select id, to_date(cast(startedTime/1000 as timestamp)) day, cluster
    from yarnHistory where startedTime>cast(%s as bigint) and
    state='FINISHED' group by id, startedTime, cluster
    """ % oneMonthAgoMillis

jobs_per_cluster_30days = sqlContext.sql(query).groupBy("day", "cluster").count()\
    .withColumnRenamed("count", "total")\
    .map(lambda row: [str(row.day), [str(row.cluster), str(row.total)]])\
    .groupByKey().map(merge).collect()

index["jobs_per_cluster_30days"] = {}
index["jobs_per_cluster_30days"]["data"] = jobs_per_cluster_30days


#
# Aggregate jobs by type for the last 30 days
#
query = """
    select id, to_date(cast(startedTime/1000 as timestamp)) day, applicationType
    from yarnHistory where startedTime>cast(%s as bigint) and state='FINISHED'
    group by id, startedTime, applicationType, cluster
    """ % oneMonthAgoMillis

app_types_last30days = sqlContext.sql(query).groupBy("day", "applicationType").count()\
    .withColumnRenamed("count", "total")\
    .map(lambda row: [str(row.day), [str(row.applicationType), str(row.total)]])\
    .groupByKey().map(merge).collect()

index["app_types_last30days"] = {}
index["app_types_last30days"]["data"] = app_types_last30days


#
# Sum jobs by type
#
total_apps = {}
for i in app_types_last30days:
    for key in i.keys():
        if not key == 'period':
            if key not in total_apps.keys():
                total_apps[key] = 0
            total_apps[key] += int(i[key])
app_types = [{"label": k, "value": v} for k, v in total_apps.items()]
index["app_types"] = {}
index["app_types"]["data"] = app_types

#
# Apps completed last 24h
#
query = """
    select state, count(*) total from(
        select id, state
        from yarnHistory where finishedTime>cast(%d as bigint) and state!='RUNNING' and state!='ACCEPTED' and state!='NEW'
        group by id, finishedTime, state, cluster
        ) T
    group by state
    """ % oneDayAgoMillis

apps_last24h = sqlContext.sql(query)\
    .map(lambda row: {"label": str(row.state), "value": str(row.total)})\
    .collect()

index["app_completed_last24"] = {}
index["app_completed_last24"]["data"] = apps_last24h

#
# Total container requested last 24h (a table row)
#

query = """select id, max(runningContainers) maxNContainers
    from yarnHistory where insertion_ts>cast(%d as bigint) and state='RUNNING'
    group by id, cluster, runningContainers
    """ % (oneDayAgoMillis/1000)

res = sqlContext.sql(query).groupBy().sum("maxNContainers").toDF("totalReqContainers")\
    .map(lambda row: {"label": "Total containers (last 24h)", "value": str(row.totalReqContainers)})\
    .collect()[0]

index["general_stats"]["data"].append(res)

#
# Total containerS questested per hour (last 24 hours) - bar chart
#
query = """
    select *, concat(day,"_",CASE WHEN length(cast(hour as string))==1 THEN concat("0",hour) ELSE hour END) hourBin from (
        select id, to_date(cast(insertion_ts as timestamp)) day, hour(cast(insertion_ts as timestamp)) hour, max(runningContainers) maxNContainers
        from yarnHistory where insertion_ts>cast(%d as bigint) and state='RUNNING'
        group by id, insertion_ts, cluster, runningContainers
    ) T
    """ % (oneDayAgoMillis/1000)

res = sqlContext.sql(query).groupBy("hourBin").sum("maxNContainers")\
    .withColumnRenamed("sum(maxNContainers)", "total")\
    .map(lambda row: {"period": row.hourBin, "value": str(row.total)})\
    .collect()

index["container_last24"] = {}
index["container_last24"]["data"] = res


#
# Total containerS questested per hour (last 24 hours) - bar chart
# 
query = """
    select *, concat(day,"_",CASE WHEN length(cast(hour as string))==1 THEN concat("0",hour) ELSE hour END) hourBin from (
        select id, to_date(cast(insertion_ts as timestamp)) day, hour(cast(insertion_ts as timestamp)) hour, max(allocatedVCores) maxAllocatedVCores
        from yarnHistory where insertion_ts>cast(%d as bigint) and state='RUNNING'
        group by id, insertion_ts, cluster, allocatedVCores
    ) T
    """ % (oneDayAgoMillis/1000)

res = sqlContext.sql(query).groupBy("hourBin").sum("maxAllocatedVCores")\
    .withColumnRenamed("sum(maxAllocatedVCores)", "total")\
    .map(lambda row: {"period": row.hourBin, "value": str(row.total)})\
    .collect()

index["vcores_last24"] = {}
index["vcores_last24"]["data"] = res

#
# Total GBs allocated per hours (last 24) - bar chart
#
query = """
    select *, concat(day,"_",CASE WHEN length(cast(hour as string))==1 THEN concat("0",hour) ELSE hour END) hourBin from (
        select id, to_date(cast(insertion_ts as timestamp)) day, hour(cast(insertion_ts as timestamp)) hour, max(allocatedMB) maxAllocatedMemory
        from yarnHistory where insertion_ts>cast(%d as bigint) and state='RUNNING'
        group by id, insertion_ts, cluster, allocatedMB
    ) T
    """ % (oneDayAgoMillis/1000)

res = sqlContext.sql(query).groupBy("hourBin").sum("maxAllocatedMemory")\
    .withColumnRenamed("sum(maxAllocatedMemory)", "total")\
    .map(lambda row: {"period": row.hourBin, "value": str(row.total/1000)})\
    .collect()

index["memoryGB_last24"] = {}
index["memoryGB_last24"]["data"] = res

#
# Long statistics (longer computation: ~ 10/20 minutes)
# If/else is needed to define behavior in case flag is false
# Currently computing:
# - max number of application running in parallel
#
if calculateLongStats:
    def mapping(row):
        return range(int(row[0]/300)*100, int(row[1]/300)*100, 300)  # one sample each 5 minutes
    
    query = """
        select id, startedTime, finishedTime from yarnHistory
        where startedTime>cast(%s as bigint) and state='FINISHED'
        group by id, startedTime, finishedTime, cluster
        """ % oneMonthAgoMillis

    max_concurrent_app = sqlContext.sql(query).repartition(10)\
        .select("startedTime", "finishedTime").flatMap(mapping)\
        .map(lambda x: [x, 1]).reduceByKey(lambda x, y: x+y).map(lambda x: x[1]).max()

else:
    max_concurrent_app = None
    for item in general_stats:
        if item["label"] == "Max simultaneous apps (last 30 days)":
            max_concurrent_app = item["value"]

index["general_stats"]["data"].append({"label": "Max simultaneous apps (last 30 days)", "value": max_concurrent_app})

#
# END long stats part
#

#
# Count number distinct users
#
query = """
    select count(distinct(user)) totalUser
    from yarnHistory
    where startedTime>cast(%s as bigint)
    """ % oneMonthAgoMillis

active_users = sqlContext.sql(query).map(lambda x: x.totalUser).collect()[0]

index["general_stats"]["data"].append({"label": "Active users (last 24h)", "value": active_users})

#
# Data for chord plots (statistics by users), last month
#
from pyspark.sql.functions import col

query = \
    """select user, cluster, count(distinct id) totalApps, applicationType
    from yarnHistory where finishedTime>cast(%d as bigint) and state='FINISHED'
    group by user, cluster, applicationType
    """ % (oneMonthAgoMillis)

res = sqlContext.sql(query).where(col("totalApps")>10)\
    .map(lambda row: {"user": row.user, "cluster": row.cluster, "totalApps":row.totalApps, "applicationType":row.applicationType})\
    .collect()

index["app_stats_chord"] = {}
index["app_stats_chord"]["data"] = res

#
# download heatman data for app duration (last 14 days)
#
#query = \
#    """select *, count(durationBin) total from (select cast(to_date(cast(startedTime/1000 as timestamp))as string) day, cast(elapsedTime/3600000 as int)+1 durationBin
#    from yarnHistory where startedTime>cast(%s as bigint) and state='FINISHED'
#    group by id, startedTime, cluster, elapsedTime ) T group by day, durationBin
#    """ % (oneMonthAgoMillis+60*60*24*14*1000) # last 14 days
#
#res = sqlContext.sql(query)\
#    .map(lambda row: {"date": row.day, "count": row.total, "bucket": row.durationBin}).collect()
#index["heatmap_duration"] = {}
#index["heatmap_duration"]["data"] = res

#
# Histogram distribution of jobs duration
#
query = \
"""select *, count(durationBin) total from (select cast(elapsedTime/3600000 as int)+1 durationBin
from yarnHistory where startedTime>cast(%s as bigint) and state!='RUNNING'
group by id, startedTime, cluster, elapsedTime ) T group by durationBin
""" % (oneMonthAgoMillis)

pprint(query)

res = sqlContext.sql(query)\
    .map(lambda row: {"count": row.total, "bucket": row.durationBin}).collect()
index["distibution_duration"] = {}
index["distibution_duration"]["data"] = res


#
# pprint index for debugging
#
pprint(index)

#
# Finally, store the JSON file
#
with open(output_file_path, 'w') as out:
    json.dump(index, out)
