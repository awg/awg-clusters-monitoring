#!/bin/bash - 
#===============================================================================
#
#          FILE: print-yarn-airflows-unscheduled-tasks.sh.sh
# 
#         USAGE: ./print-yarn-airflows-unscheduled-tasks.sh.sh
# 
#   DESCRIPTION: considering tasks older than one hour
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 09/11/2016 12:00
#      REVISION:  ---
#===============================================================================

table=$(sqlite3 -csv /root/airflow/airflow.db 'select total, REPLACE(execution_date," ","T") execution_date, executed_task_id from (select GROUP_CONCAT(task_id, ",") executed_task_id, execution_date, count(task_id) total from task_instance where dag_id = "yarn_logs_aggregation_v1.4" group by execution_date) T where T.total != 21;' | tr "\n" "|")

python -c """
import sys;
from datetime import datetime, timedelta;

def check_task(task, list, ts):
	if task not in list:
		print 'airflow run yarn_logs_aggregation_v1.4', task, ts

table = sys.argv[1]
lines = table.split('|')
for line in lines:
	if len(line.split(',')) >= 3:
		items=line.split(',')
		ts = (items[1])[:-7]
		if (datetime.now()-timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%S') > ts:
			check_task('merge_files_analytix',items,ts)
			check_task('merge_files_hadalytic',items,ts)
			check_task('merge_files_lxhadoop',items,ts)
			check_task('create_tar_analytix',items,ts)
			check_task('create_tar_hadalytic',items,ts)
			check_task('create_tar_lxhadoop',items,ts)
			check_task('move_tar_analytix',items,ts)
			check_task('move_tar_hadalytic',items,ts)
			check_task('move_tar_lxhadoop',items,ts)
			check_task('hadoop_put_analytix',items,ts)
			check_task('hadoop_put_hadalytic',items,ts)
			check_task('hadoop_put_lxhadoop',items,ts)
			check_task('remove_logs_analytix',items,ts)
			check_task('remove_logs_hadalytic',items,ts)
			check_task('remove_logs_lxhadoop',items,ts)
			check_task('remove_merged_logs_analytix',items,ts)
			check_task('remove_merged_logs_hadalytic',items,ts)
			check_task('remove_merged_logs_lxhadoop',items,ts)
			check_task('remove_tar_analytix',items,ts)
			check_task('remove_tar_hadalytic',items,ts)
			check_task('remove_tar_lxhadoop',items,ts)
			print 'airflow backfill -s %s -e %s yarn_logs_aggregation_v1.4' %(ts,ts)
""" $table