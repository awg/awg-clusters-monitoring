d3.json("data/consolidated.json", function(error, statistics) {

  var keys = Object.keys(statistics)
  // console.log(statistics[keys[0]])
  var headers = Object.keys(statistics[keys[0]])

  var table = d3.select('table[id="dataTables"]')
  var header = table.append("thead").append("tr")
  headers.forEach(
    function(element){
      if ( element !== "numFiles" && element !== "diskUsage" )
        header.append("th").text(element)
    }
  )
  header.append("th").text("numFiles")
  header.append("th").text("diskUsage")

  var body = table.append("tbody")

  keys.forEach(
    function(key){
      tr = body.append("tr")
      headers.forEach(
        function(element) {
          if ( element !== "numFiles" && element !== "diskUsage" )
            tr.append("td").text(statistics[key][element])
        }
      )
      tr.append("td").text(JSON.stringify(statistics[key]["numFiles"]))
      tr.append("td").text(JSON.stringify(statistics[key]["diskUsage"]))
    }
  )

  analytixColour = '#c5491d'
  hadalyticColour = '#b3dd2a'
  lxhadoopColour = '#1e54aa'

  var lastApps = d3.select('div[id="space"]')
  lastApps.select('div[id="loading-bar"]').remove()

  diskUsage = []
  keys.forEach(
    function(key){
      var d = statistics[key]["diskUsage"]
      var quota = statistics[key]["quota"] || undefined
      if (quota === "-")
        quota = undefined
      result = {
        project:key,
      }
      if (quota != undefined){
        quota = quota*1024*1024*1024*1024
        result['quota'] = quota
      }
      if (d['p01001532975913.cern.ch'] != undefined )
        result['analytix']=d['p01001532975913.cern.ch']
      if (d['p01001532067275.cern.ch'] != undefined )
        result['hadalytic']=d['p01001532067275.cern.ch']
      if (d['p01001533959135.cern.ch'] != undefined )
        result['lxhadoop']=d['p01001533959135.cern.ch']
      diskUsage.push(result)
    }
  )

  if (statistics) {
    Morris.Bar({
        element: 'space',
        data: diskUsage,
        xkey: 'project',
        ykeys: ['analytix', 'hadalytic', 'lxhadoop', 'quota'],
        labels: ['analytix', 'hadalytic', 'lxhadoop', 'quota'],
        barColors: [analytixColour, hadalyticColour, lxhadoopColour, '#000'],
        hideHover: 'auto',
        resize: true
    });
  } else {
    lastApps.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  var lastApps = d3.select('div[id="files"]')
  lastApps.select('div[id="loading-bar"]').remove()

  numFiles = []
  keys.forEach(
    function(key){
      var d = statistics[key]["numFiles"]
      numFiles.push({project:key,
        analytix:d['p01001532975913.cern.ch'],
        hadalytic:d['p01001532067275.cern.ch'],
        lxhadoop:d['p01001533959135.cern.ch']
      })
    }
  )

  if (statistics) {
    Morris.Bar({
        element: 'files',
        data: numFiles,
        xkey: 'project',
        ykeys: ['analytix', 'hadalytic', 'lxhadoop'],
        labels: ['analytix', 'hadalytic', 'lxhadoop'],
        barColors: [analytixColour, hadalyticColour, lxhadoopColour],
        hideHover: 'auto',
        resize: true
    });
  } else {
    lastApps.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }



});