#!/bin/bash - 
#===============================================================================
#
#          FILE: yarn-logs.sh
# 
#         USAGE: ./yarn-logs.sh
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Luca Menichetti (lmeniche), luca.menichetti@cern.ch
#  ORGANIZATION: CERN
#       CREATED: 16/03/2017 12:00
#      REVISION:  ---
#===============================================================================

CLUSTER_NAME=hadalytic
NAMENODE_HOSTNAME=p01001532067275

python parseYarnAppsMetrics.py $CLUSTER_NAME "http://$NAMENODE_HOSTNAME.cern.ch:8088/ws/v1/cluster/apps?state=RUNNING"

# finishedTimeEnd=`date --date="$(date +'%H:00')" +'%s'`000
# finishedTimeBegin=`date --date="$(date +'%H:00' --date '1 hours ago')" +'%s'`000

# python parseYarnAppsMetrics.py $CLUSTER_NAME "http://$NAMENODE_HOSTNAME.cern.ch:8088/ws/v1/cluster/apps?finishedTimeEnd=$finishedTimeEnd&finishedTimeBegin=$finishedTimeBegin"
