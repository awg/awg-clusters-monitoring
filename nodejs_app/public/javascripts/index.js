/*
 * function: bytes to human readable size   
 */
var humanFileSize = function(bytes, si) {
  var thresh = si ? 1000 : 1024;
  if(Math.abs(bytes) < thresh) {
    return bytes + ' B';
  }
  var units = si
    ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
    : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
  var u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while(Math.abs(bytes) >= thresh && u < units.length - 1);
  return bytes.toFixed(1)+' '+units[u];
}

/*
 * Read stats and draw them
 */
d3.json("data/index.json", function(error, statistics) {

  if (error) {
      // return;
  }

  var overviewNode = d3.select('div[id="clusters-overview"]')

  overviewNode.select('div[id="loading-bar"]').remove()

  Object.keys(statistics.overview)
  .map(function(elem){return[elem == "nodes" && 1 || 
        elem == "vcores" && 2 || 
        elem == "RAM" && 3 || 
        elem == "HHD" && 4 || 
        elem == "space" && 5 || 
        elem == "daily_jobs" && 6,elem]})
  .sort()
  .map(function(elem){return elem[1]}) 
  .forEach(
    function(stat){

      panelType = 
        stat == "nodes" && "panel-primary" || 
        stat == "vcores" && "panel-warning" || 
        stat == "RAM" && "panel-info" || 
        stat == "HHD" && "panel-green" || 
        stat == "space" && "panel-yellow" || 
        stat == "daily_jobs" && "panel-red" 

      icon =
        stat == "nodes" && "icon-servers" || 
        stat == "vcores" && "icon-cpu-processor" || 
        stat == "RAM" && "icon-ram" || 
        stat == "HHD" && "fa-hdd-o" || 
        stat == "space" && "fa-floppy-o" || 
        stat == "daily_jobs" && "fa-tasks"         

      value = 
        stat == "nodes" && statistics.overview[stat] || 
        stat == "vcores" && statistics.overview[stat] || 
        stat == "RAM" && humanFileSize(statistics.overview[stat]*1000,true) ||
        stat == "HHD" && statistics.overview[stat] || 
        stat == "space" && humanFileSize(statistics.overview[stat]*1000,true) || 
        stat == "daily_jobs" &&  statistics.overview[stat]

      text =
        stat == "nodes" && "Total Nodes" || 
        stat == "vcores" && "Total VCores" || 
        stat == "RAM" && "Total RAM" ||
        stat == "HHD" && "Total HDD" || 
        stat == "space" && "Storage Space" || 
        stat == "daily_jobs" && "Jobs per day" 

      var rowNode = overviewNode
        .append('div').attr('class', 'col-lg-2 col-md-6')
        .append('div').attr('class', 'panel '+panelType)
        .append('div').attr('class', 'panel-heading')
        .append('div').attr('class', 'row')

      rowNode
        .append('div').attr('class', 'col-xs-3')
        .append('i').attr('class', 'fa '+icon+' fa-4x')
      
      content = rowNode
        .append('div').attr('class', 'col-xs-9 text-right')
      
      content.append('div').attr('class','huge').text(value)
      content.append('div').text(text)
    }
  )

  analytixColour = '#c5491d'
  hadalyticColour = '#b3dd2a'
  lxhadoopColour = '#1e54aa'

  var lastApps = d3.select('div[id="apps-per-month"]')
  lastApps.select('div[id="loading-bar"]').remove()

  if (statistics.jobs_per_cluster_30days) {
    Morris.Line({
        element: 'apps-per-month',
        data: statistics.jobs_per_cluster_30days.data,
        xkey: 'period',
        ykeys: ['analytix', 'hadalytic', 'lxhadoop'],
        labels: ['analytix', 'hadalytic', 'lxhadoop'],
        pointSize: 5,
        lineWidth: 1,
        smooth: false,
        hideHover: 'auto',
        lineColors: [analytixColour, hadalyticColour, lxhadoopColour],
        resize: true
    });
  } else {
    lastApps.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Table with statistics
   */

  var statsTable = d3.select('div[id="stats-table"]')
  statsTable.select('div[id="loading-bar"]').remove()

  table = statsTable
    .append('div').attr('class', 'table-responsive')
    .append('table').attr('class', 'table table-striped table-hover')
    .append('tbody')

  statistics.general_stats.data.forEach(function(row) {
    tr = table.append('tr')
      tr.append('td').text(row.label)
      tr.append('td').text(row.value)
  })

  updatedTS = new Date(statistics.ts*1000)
  tr = table.append('tr')
    tr.append('td').text("Stats update timestamp")
    tr.append('td').text(updatedTS.getFullYear()+"-"+(updatedTS.getMonth()+1)+"-"+updatedTS.getDate()+" "+updatedTS.getHours()+":"+updatedTS.getMinutes())

  /*
   * Pie chart app types
   */

  var appTypesPie = d3.select('div[id="applications-types"]')
  appTypesPie.select('div[id="loading-bar"]').remove()

  if (statistics.jobs_per_cluster_30days) {
    Morris.Donut({
      element: 'applications-types',
      data: statistics.app_types.data,
      colors: [analytixColour, lxhadoopColour],
      resize: true
    });
  } else {
    appTypesPie.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Bar chart, app types last 30 days
   */
  var appTypesChart = d3.select('div[id="app-types-per-day"]')
  appTypesChart.select('div[id="loading-bar"]').remove()
  sorted = statistics.app_types_last30days.data.sort(function(el1,el2){return new Date(el1["period"])-new Date(el2["period"])})

  if (statistics.app_types_last30days) {
    Morris.Bar({
        element: 'app-types-per-day',
        data: sorted,
        xkey: 'period',
        ykeys: ['SPARK', 'MAPREDUCE'],
        labels: ['SPARK', 'MAPREDUCE'],
        barColors: [analytixColour, lxhadoopColour],
        hideHover: 'auto',
        resize: true
    });
  } else {
    appTypesChart.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Bar chart, app STATUS last 30 days
   */
  var appStatusChart = d3.select('div[id="app-status-per-day"]')
  appStatusChart.select('div[id="loading-bar"]').remove()
  sorted = statistics.jobs_status_30days.data.sort(function(el1,el2){return new Date(el1["period"])-new Date(el2["period"])})

  if (statistics.jobs_status_30days) {
    Morris.Bar({
        element: 'app-status-per-day',
        data: sorted,
        stacked: true,
        xkey: 'period',
        ykeys: ['FINISHED', 'FAILED', 'KILLED'],
        labels: ['SUCCEEDED', 'FAILED', 'KILLED'],
        barColors: ['#888B30', '#951A12' , '#000000'],
        hideHover: 'auto',
        resize: true
    });
  } else {
    appStatusChart.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Pie chart HDFS usage
   */

  var appTypesPie = d3.select('div[id="hdfs-usage"]')
  appTypesPie.select('div[id="loading-bar"]').remove()

  if (statistics.hdfs_usage) {
    Morris.Donut({
      element: 'hdfs-usage',
      data: statistics.hdfs_usage.data,
      formatter: function(y, data){ return y+" %"},
      resize: true
    });
  } else {
    appTypesPie.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Bar chart, count requested containers per hour
   */
  var containtersStatsChart = d3.select('div[id="containers-stats"]')
  containtersStatsChart.select('div[id="loading-bar"]').remove()
  sorted = statistics.container_last24.data
    .map(function(el) {
      [date,hour] = el["period"].split("_");
      [year,month,day] = date.split("-")
      return {"date": new Date(year,month-1,day,hour), "total": el["value"] , "period": el['period'].replace('_',' ')+'h'}}
    )
    .sort(function(el1,el2){return el1["date"]-el2["date"]})

  if (statistics.container_last24 !== undefined) {
    Morris.Bar({
        element: 'containers-stats',
        data: sorted,
        stacked: true,
        xkey: 'period',
        ykeys: ['total'],
        labels: ['total'],
        hideHover: 'auto',
        resize: true
    });
  } else {
    appStatusChart.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Bar chart, count requested containers per hour
   */
  var containtersStatsChart = d3.select('div[id="vcores-stats"]')
  containtersStatsChart.select('div[id="loading-bar"]').remove()
  sorted = statistics.vcores_last24.data
    .map(function(el) {
      [date,hour] = el["period"].split("_");
      [year,month,day] = date.split("-")
      return {"date": new Date(year,month-1,day,hour), "total": el["value"] , "period": el['period'].replace('_',' ')+'h'}}
    )
    .sort(function(el1,el2){return el1["date"]-el2["date"]})

  if (statistics.vcores_last24) {
    Morris.Bar({
        element: 'vcores-stats',
        data: sorted,
        stacked: true,
        xkey: 'period',
        ykeys: ['total'],
        labels: ['total'],
        hideHover: 'auto',
        resize: true
    });
  } else {
    appStatusChart.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Bar chart, count requested memory (GB) per hour (last 12)
   */
  var containtersStatsChart = d3.select('div[id="memory-stats"]')
  containtersStatsChart.select('div[id="loading-bar"]').remove()
  sorted = statistics.memoryGB_last24.data
    .map(function(el) {
      [date,hour] = el["period"].split("_");
      [year,month,day] = date.split("-")
      return {"date": new Date(year,month-1,day,hour), "total": el["value"] , "period": el['period'].replace('_',' ')+'h'}}
    )
    .sort(function(el1,el2){return el1["date"]-el2["date"]})

  if (statistics.memoryGB_last24) {
    Morris.Bar({
        element: 'memory-stats',
        data: sorted,
        stacked: true,
        xkey: 'period',
        ykeys: ['total'],
        labels: ['total GB'],
        hideHover: 'auto',
        resize: true
    });
  } else {
    appStatusChart.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

  /*
   * Add chord plots
   */
  var users = {};
  statistics.app_stats_chord.data.forEach(
    function(d) {
      if (!users[d.user]){
        users[d.user]={}
      }
      users[d.user][d.applicationType]=(users[d.user][d.applicationType]|0)+d.totalApps
      users[d.user][d.cluster]=(users[d.user][d.cluster]|0)+d.totalApps
    }
  );

  var chord1DOM = d3.select('div[id="chord1"]')
  chord1DOM.select('div[id="loading-bar"]').remove()
  chord1(users, d3)

  var chord2DOM = d3.select('div[id="chord2"]')
  chord2DOM.select('div[id="loading-bar"]').remove()
  chord2(users, d3)

  /*
   * jobs duration histogram distribution
   */
  var histogramDurationDOM = d3.select('div[id="duration-dist"]')
  histogramDurationDOM.select('div[id="loading-bar"]').remove()

  if (statistics.distibution_duration) {
    Morris.Line({
        element: 'duration-dist',
        data: statistics.distibution_duration.data
          .sort(function(el1,el2){return el1["bucket"]-el2["bucket"]})
          .map(function(el){el["totalLog"]=Math.log10(el["count"]); el["bucket"]=el["bucket"]*1000*60;return el}),
        xkey: 'bucket',
        xLabelFormat: function (x) { return x/1000/60+" h"; },
        yLabelFormat: function (x) { var v = Math.pow(10,x); return v.toString(); },
        pointSize: 3,
        lineWidth: 1,
        pointFillColors: ['#ffffff'],
        pointStrokeColors: ['#003a04'],
        lineColors: ['#003a04'],
        smooth: false,
        ykeys: ['totalLog'],
        labels: ['total'],
        hideHover: 'auto',
        hoverCallback: function (index, options, content, row) {
          return "Duration: <b>"+ row.bucket/1000/60 +" hour"+(row.bucket/1000/60!==1&&"s"||"")+"</b></br>"+
            "Total jobs: <b>"+row.count+"</b>"
        },
        resize: true
    }) && histogramDurationDOM.select('div[id="desc"]').style("visibility","").style("height","");
  } else {
    histogramDurationDOM.append("div").attr('class', 'alert alert-danger').text('ERROR loading plot. Please contact an admin.')
  }

});
